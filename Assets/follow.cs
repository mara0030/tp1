using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follow : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 c = transform.position;

        Vector3 S = GameObject.Find("Player").transform.position;

        Vector3 Vsc = S - c;

        float vitesse = 0.01f;

        Vector3 newPosition = c + vitesse * Vsc/Vsc.magnitude;

        transform.position = newPosition;

        transform.LookAt(S);
    }
}
