using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{

    void Start()
    {
        
    }


   void Update()
{

    if (Input.GetKey(KeyCode.Z))
    {

        transform.Translate(Vector3.forward * 0.01f);
    }
    if (Input.GetKey(KeyCode.S))
    {

        transform.Translate(Vector3.back * 0.01f);
    }


    if (Input.GetKey(KeyCode.Q))
    {

        transform.Rotate(Vector3.up, -2);
    }
    if (Input.GetKey(KeyCode.D))
    {

        transform.Rotate(Vector3.up, 2);
    }

    if (Input.GetKey(KeyCode.Space))
    {
        transform.Translate(Vector3.up * 0.01f);
    }

    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Z))
    {
        transform.Translate(Vector3.forward * 0.02f);
    }
}


}
